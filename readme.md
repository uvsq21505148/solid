Exercice 1 SRP (Single responsibility principle)

Une classe ne devrait avoir qu'une unique raison pour être modifiée. Une responsabilité est une cause de changement.

Question 1

Ici, la classe Employe possède deux causes de changement / responsabilités : calculSalaire et afficheCoordonnees

Question 2

Si la méthode afficheCoordonnees change, il faut recompiler toute la classe Employe pour faire fonctionner le code.

Question 3

Même problème que la question 2, les deux fonctions sont toujours couplées.

Exercice 2 OCP (Open-Closed principle)

Une classe doit être ouverte aux extensions et fermée aux modifications. On doit pouvoir ajouter des fonctionnalités sans modifier le code existant.

On utilise le polymorphisme pour répondre au problème.

Exercice 3 (Liskov substitution principle)

On doit pouvoir remplacer un objet de la classe mère par un objet de la classe fille sans que le comportement du programme ne change.

Question 1

Classe mère : Robot, Classe fille : RobotStatique
Si on remplace un robot par un robot statique le programme ne va pas fonctionner et lancer une exception

Question 2 - 3

On remplace classe mère et fille, RobotMobile devient la classe fille et RobotStatique devient la classe fille.

Pour coder la méthode avancerTous, il nous faut une collection de RobotMobile uiquement ainsi le code ne lancera pas d'exception.

Exercice 4 (Interface segregation principle)

Faire des interfaces contenant uniquement les fonctionnalités attendues par le client.

Question 1

La classer SimplePrinter va dépendre de comportement qui ne l'intéresse pas (fax, scan, copy).

Question 2

La modification de ces comportements va forcer la mise a jour / recompilation de SimplePrinter alors qu'à la base il n'en à pas besoin.

Exercice 5 (Dependancies inversion principle)

Les modules de haut-niveau ne devraient pas dépendre de modules de bas niveau.
Les deux devraient dépendre de concepts abstraits.
On ne doit pas dépendre de détails dans les abstractions.

Question 1

La méthode métier est un concept de haut-niveau et dépend de détails : "LocalDateTime.now()"
On doit créer une classe Controller qui va dépendre de la classe métier et gérer les détails.
