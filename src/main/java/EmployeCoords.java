public class EmployeCoords {
	private Employe employe;
	
	public EmployeCoords(String nom, String adresse) {
		employe = new Employe(nom, adresse);
	}
	
	public String afficheCoords() {
		return employe.getNom() + ", " + employe.getAdresse();
	}
}
