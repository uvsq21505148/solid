public interface Printer {
	void print();
	void scan();
	void copy();
	void fax();
}
