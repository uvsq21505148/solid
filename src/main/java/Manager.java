public class Manager extends EmployeOCP{
	private int nbEmp;
	
	public Manager(int anciennete, int nbEmp) {
		super(anciennete);
		this.nbEmp = nbEmp;
	}
	
	@Override
	public double calculSalaire() {
		return 1500 + 20 * anciennete + 100 * nbEmp;
	}
}
