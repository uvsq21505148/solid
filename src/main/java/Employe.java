public class Employe {
	private final String nom;
	private final String adresse;
	
	public Employe(String nom, String adresse) {
		this.adresse = adresse;
		this.nom = nom;
	}
	
	public String getNom() { return nom; }
	public String getAdresse() { return adresse; }
}
