public class EmployeOCP {
	protected int anciennete;
	
	public EmployeOCP(int anciennete) {
		this.anciennete = anciennete;
	}
	
	public double calculSalaire() {
		return 1500 + 20 * anciennete;
	}
}
