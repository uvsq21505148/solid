public class Vendeur extends EmployeOCP{
	private int commission;
	
	public Vendeur(int anciennete, int commission) {
		super(anciennete);
		this.commission = commission;
	}
	
	@Override
	public double calculSalaire() {
		return 1500 + 20 * anciennete + commission;
	}
}
