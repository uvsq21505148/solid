import java.util.ArrayList;

public class ExosPartie3 {
	
	public static void main(String[] args) {
		//exo1();
		//exo2();
		//exo4();
		exo5();
	}
	
	public static void exo1() {
		EmployeCoords ec = new EmployeCoords("a", "b");
		EmployeSalaire es = new EmployeSalaire("c", "d");
		System.out.println("salaire " + es.calculSalaire());
		System.out.println("coords " + ec.afficheCoords());
	}
	
	public static void exo2() {
		EmployeOCP e = new EmployeOCP(12);
		Vendeur v = new Vendeur(5, 100);
		Manager m = new Manager(10, 5);
		
		ArrayList<EmployeOCP> entreprise = new ArrayList<EmployeOCP>();
		entreprise.add(e);
		entreprise.add(v);
		entreprise.add(m);
		
		double total = 0;
		for(EmployeOCP tmp : entreprise) {
			total += tmp.calculSalaire();
		}
		System.out.println("Salaire total entreprise " + total);
	}
	
	public static void exo4() {
		SimplePrinter sp = new SimplePrinter();
		sp.print();
	}
	
	public static void exo5() {
		UneClasseMetier ucm = new UneClasseMetier();
		MetierController mc = new MetierController(ucm);
		
		mc.loggingMethodeMetier();
	}
}
