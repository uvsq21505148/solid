import java.time.LocalDateTime;

public class MetierController {
	
	public UneClasseMetier ucm;
	
	public MetierController(UneClasseMetier ucm){
		this.ucm = ucm;
	}
	
	public void loggingMethodeMetier() {
		System.out.println(LocalDateTime.now() + "Debut de une methode metier");
		ucm.uneMethodeMetier();
		System.out.println(LocalDateTime.now() + "Fin de une methode metier");
		
	}
}
